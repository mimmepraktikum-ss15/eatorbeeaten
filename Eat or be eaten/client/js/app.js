var App = App || {};

App.app = (function () {
    "use strict";
    /* eslint-env browser, jquery  */

    var playerName,
        playerNameInput = document.getElementById("playerNameInput"),
        socket,
        screenWidth = window.innerWidth,
        screenHeight = window.innerHeight,
        c = document.getElementById("cvs"),
        canvas = c.getContext("2d"),
        KEY_ENTER = 13,
        game;
    c.width = screenWidth;
    c.height = screenHeight;
    
    var gameConfig = {
        Speed: 3,
        startRadius: 30,
        gameStatus: "startscreen"
    };
    
    var mouse = {
        xCoordinate: -1,
        yCoordinate: -1,
    };
    
    var player = {
        name: playerName,
        selectedSkin: "",
        growthFactor: 0.5,
        mass: 0,
        playerRadius: gameConfig.startRadius,
        playerColor: "",
        playerDistance: 0,
        playerMovementSpeed: gameConfig.Speed,
        playerXspeed: 0,
        playerYspeed: 0,
        playerXcoord: window.innerWidth/2,
        playerYcoord: window.innerHeight/2,
        playerStatus: "alive"
    };


    function startGame() {
        gameConfig.gameStatus = "playing";
        c.addEventListener("mousemove", onMouseMove);
        playerName = playerNameInput.value.replace(/(<([^>]+)>)/ig, "");
        document.getElementById("gameAreaWrapper").style.display = "block";
        document.getElementById("startMenuWrapper").style.display = "none";
        socket = io();
        setupSocket(socket);
        animloop();
    }
    
    //Verarbeitet das Mouse Move Event.
    function onMouseMove(event) {
        game.handleMouseEvent(event);
    }

// check if nick is valid alphanumeric characters (and underscores)
    function validNick() {
        var regex = /^\w*$/;
        //console.log("Regex Test", regex.exec(playerNameInput.value));
        return regex.exec(playerNameInput.value) !== null;
    }

    window.onload = function () {
        "use strict";
        var btn = document.getElementById("startButton"),
            nickErrorText = document.querySelector("#startMenu .input-error"),
            settingsSelect = document.getElementById("settingsselect");

        btn.onclick = function () {
            player.selectedSkin = settingsSelect.value;
        // check if the nick is valid
            if (validNick()) {
                startGame();
            } else {
                nickErrorText.style.display = "inline";
            }
        };
        
        var settingsBtn = document.getElementById("settingsButton");
        var settingsList = document.getElementById("settingslist");
        var helpList = document.getElementById("help");

        settingsBtn.onclick = function () {
            if (settingsList.style.maxHeight == '300px') {
                settingsList.style.maxHeight = '0px';
            } else {
                settingsList.style.maxHeight = '300px';
            }
        };
        
        playerNameInput.addEventListener("keypress", function (e) {
            var key = e.which || e.keyCode;

            if (key === KEY_ENTER) {
                if (validNick()) {
                    startGame();
                } else {
                    nickErrorText.style.display = "inline";
                }
            }
        });
    };

    function setupSocket(socket) {
        game.handleNetwork(socket);
    }

    window.requestAnimFrame = (function () {
        return window.requestAnimationFrame       ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame    ||
                function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
    })();

    function animloop() {
        requestAnimFrame(animloop);
        gameLoop();
    }

    function gameLoop() {
        game.handleLogic();
        game.handleGraphics(canvas, player.selectedSkin);
    }
    
    function getPlayerParameters() {
        return player;
    };
    
    function getMouseParameters() {
        return mouse;
    };

    window.addEventListener("resize", function () {
        screenWidth = window.innerWidth;
        screenHeight = window.innerHeight;
        c.width = screenWidth;
        c.height = screenHeight;
    }, true);  
    
    window.addEventListener("keydown", function() {
        if(gameConfig.gameStatus == "playing") {
        game.onKeyDown(event);
        }
    });
                            
    function init() {
        game = new App.game();
    }

    return {
        init: init,
        setupPlayerParameters: getPlayerParameters,
        setupMouseParameters: getMouseParameters
    };
}());