var App = App || {};

App.game = function () {
    "use strict";
    /* eslint-env jquery */
    
    var massParticles = [];
    
    var player = {};
    
    var mouse = {};
    
    var enemies = [];
    
    var massEject = [];

    function handleNetwork(socket) {
        //console.log("Game connection process here");
        //console.log(socket);
        //socket.emit('disconnect') to trigger disconnect event in server
  // This is where you receive all socket messages
    }

    function handleLogic() {
        handlePlayerMovement();
    }

    //Hier wird alles gezeichnet.
    function handleGraphics(gfx) {
        gfx.clearRect(0,0, window.innerWidth, window.innerHeight);
        drawParticles(gfx, massParticles);
        drawEnemies(gfx);
        drawMassEject(gfx);
        drawPlayer(gfx);
    }
    
    /*
    Diese Funktion zeichnet den Masse ausstoß. 
    TODO: Größe des Ausstoß nicht als Magic Number!
    */
    function drawMassEject(canvas) {
        for(var i = 0; i<massEject.length; i++) {
            canvas.beginPath();
            canvas.arc(massEject[i].Xcoord, massEject[i].Ycoord, 5, 0, 2*Math.PI, false);
            canvas.fillStyle = massEject[i].color;
            canvas.fill();
            canvas.strokeStyle = "#FFFFFF";
            canvas.stroke();
            canvas.closePath();
        };
    };
    
    function handlePlayerMovement() {
        if(player.playerStatus == "dead") {
            return;
        };
        var xDifference = getCoordinateDifference(mouse.xCoordinate, player.playerXcoord);
        var yDifference = getCoordinateDifference(mouse.yCoordinate, player.playerYcoord);
        var score = document.getElementById("score");
        if(player.distance < 0.0001) {
            player.distance = player.distance;
        }else{
        player.distance = getDistance(xDifference, yDifference);
        }
        if(player.distance < 0.0001) {
            player.playerXcoord = player.playerXcoord;
            player.playerYcoord = player.playerYcoord;
        }else{
            if(player.distance < player.playerRadius) {
                var slowIndex = player.distance / player.playerRadius;
                player.playerXcoord = player.playerXcoord + (player.playerXspeed * slowIndex);
                player.playerYcoord = player.playerYcoord + (player.playerYspeed * slowIndex);
                }else{
                    player.playerXcoord = player.playerXcoord + player.playerXspeed;
                    player.playerYcoord = player.playerYcoord + player.playerYspeed;
                }
        }
        checkCollision();
        score.innerHTML = "Your Score: "+ player.mass;
    };
    
        
    function drawEnemies(canvas) {
        for(var i = 0; i<enemies.length; i++) {
            canvas.beginPath();
            canvas.arc(enemies[i].enemyXcoord, enemies[i].enemyYcoord, enemies[i].enemyRadius, 0, 2*Math.PI, false);
            canvas.fillStyle = enemies[i].enemyColor;
            canvas.fill();
            canvas.strokeStyle = "#FFFFFF";
            canvas.stroke();
            canvas.closePath();
        }
    };
    
    function drawPlayer(canvas) {
        if(player.playerStatus == "alive") {
        canvas.beginPath();
        canvas.arc(player.playerXcoord, player.playerYcoord,player.playerRadius,0,2*Math.PI,false);
        if(player.selectedSkin != "Standard") {
            clippedBackgroundImage(canvas);
            }else{
                canvas.fillStyle = player.playerColor;
                canvas.fill();
            }
        canvas.strokeStyle = "#000000";
        canvas.stroke();
        canvas.closePath();
        }else{
            console.log("DEAD");
        }
        //console.log(mouse.xCoordinate);
        //console.log(mouse.yCoordinate);
    };
    
    function clippedBackgroundImage(canvas) {
        var skinImg = new Image(),
            skinHeight,
            skinImgXCoord = player.playerXcoord ,
            skinImgYCoord = player.playerYcoord ;
        
        
        canvas.save();
        canvas.clip();
        switch(player.selectedSkin) {
            case "Earthskin":
                skinImg.src = "http://agarioskins.com/img/skin/earth.png";
                canvas.fillStyle = "#000";
                canvas.fill();
                skinHeight = player.playerRadius*2 / skinImg.width * skinImg.height;
                canvas.drawImage(skinImg, skinImgXCoord - (skinHeight/2), skinImgYCoord - (skinHeight/2), skinHeight, skinHeight);
                break;
            case "Germanyskin":
                skinImg.src = "http://www.agarioguide.com/wp-content/uploads/2015/05/germany.png";
                canvas.fillStyle = "#000";
                canvas.fill();
                skinHeight = player.playerRadius*2 / skinImg.width * skinImg.height;
                canvas.drawImage(skinImg, skinImgXCoord - (skinHeight/2), skinImgYCoord - (skinHeight/2), skinHeight, skinHeight);
                break;
            case "Faceskin":
                skinImg.src = "http://agarioskins.com/img/skin/doge.png";
                canvas.fillStyle = "#000";
                canvas.fill();
                skinHeight = player.playerRadius*2 / skinImg.width * skinImg.height;
                canvas.drawImage(skinImg, skinImgXCoord - (skinHeight/2), skinImgYCoord - (skinHeight/2), skinHeight, skinHeight);
                break;
            case "Pokeballskin":
                skinImg.src = "http://i.imgur.com/FMkhwtw.jpg";
                canvas.fillStyle = "#000";
                canvas.fill();
                skinHeight = player.playerRadius*2 / skinImg.width * skinImg.height;
                canvas.drawImage(skinImg, skinImgXCoord - (skinHeight/2), skinImgYCoord - (skinHeight/2), skinHeight, skinHeight);
                break;
                
        }
        canvas.restore();
        
    };
    
    function checkCollision() {
        checkParticleCollision();
        checkEnemyCollision();
    };
    
    function checkEnemyCollision() {
        for(var i = 0; i<enemies.length; i++) {
            var enemyXcoord = enemies[i].enemyXcoord,
                enemyYcoord = enemies[i].enemyYcoord,
                enemyRadius = enemies[i].enemyRadius,
                enemyMass = enemies[i].enemyMass,
                deltaX = getCoordinateDifference(enemyXcoord, player.playerXcoord),
                deltaY = getCoordinateDifference(enemyYcoord, player.playerYcoord),
                distance = getDistance(deltaX, deltaY);
            if(distance < enemyRadius + player.playerRadius) {
                if(player.playerRadius > enemyRadius) {
                    //player radius größer, player gewinnt fight
                    if(distance < player.playerRadius) {
                        enemies.splice(i, 1);
                        grantPlayerMass(enemyMass);
                    }
                }else{
                    //enemy radius größer, enemy gewinnt fight
                    if(distance < enemyRadius) {
                        player.playerStatus = "dead";
                    }
                }
            }
            //no enemy contact
        }
    };
        
    function checkParticleCollision() {
        for(var i = 0; i < massParticles.length; i++) {
            var particleXcoord = massParticles[i].Xcoord;
            var particleYcoord = massParticles[i].Ycoord;
            var deltaX = getCoordinateDifference(particleXcoord, player.playerXcoord);
            var deltaY = getCoordinateDifference(particleYcoord, player.playerYcoord);
            var distance = getDistance(deltaX, deltaY);
            if(distance < player.playerRadius) {
                massParticles.splice(i, 1);
                //console.log("SPLICE");
                grantPlayerMass(1);
            };
        };
    };
    
    function grantPlayerMass(mass) {
        player.playerRadius += player.growthFactor * mass;
        player.mass += mass;
    };
    
    function setupPlayerParameters() {
        player = App.app.setupPlayerParameters();
        player.playerColor = randomColor();
    };
    
    function setupMouseParameters() {
        mouse = App.app.setupMouseParameters();
    };
    
    
    function handleMouseMovement(event) {
        console.log("handleMouseMovement!");
        var playerXcoordinate,
            playerYcoordinate,
            playerSpeed,
            mouseXcoordinate,
            mouseYcoordinate;
        
        playerXcoordinate = player.playerXcoord;
        playerYcoordinate = player.playerYcoord;
        playerSpeed = player.playerMovementSpeed;
        mouseXcoordinate = event.clientX;
        mouseYcoordinate = event.clientY; 
        var deltaX = getCoordinateDifference(mouseXcoordinate, playerXcoordinate);
        var deltaY = getCoordinateDifference(mouseYcoordinate, playerYcoordinate);
        var distance = getDistance(deltaX, deltaY);
        var speedFactor = getSpeedFactor(distance, playerSpeed);
        var xSpeed = getSpeed(deltaX, speedFactor);
        var ySpeed = getSpeed(deltaY, speedFactor);        
        player.playerXspeed = xSpeed;
        player.playerYspeed = ySpeed;
        player.distance = distance;
        updateMouseCoordinates(mouseXcoordinate, mouseYcoordinate);
        
    }    
    function updateMouseCoordinates(x,y) {
        mouse.xCoordinate = x;
        mouse.yCoordinate = y;
    };
    
    function getCoordinateDifference(FirstCoord, SecondCoord) {
        return FirstCoord - SecondCoord;
    }
    function getDistance(dx, dy) {
        return Math.sqrt(dx*dx+dy*dy);
    }
    function getSpeedFactor(distance, speed) {
        return distance / speed;
    }
    function getSpeed(delta, factor) {
        return delta/factor;
    }
    
    
    
       //erzeugt Partikelobjekte mit den Paramtern x-Koordinate, y-Koordinate und eine generierte Farbe und speichert diese in massParticles.
    function createMassParticles() {
        for(var i = 0; i<=200; i++) {
        var particle = {Xcoord: Math.random() * 1280, Ycoord: Math.random() * 1280, color: randomColor()};
        massParticles.push(particle);
        }
    }
    
      function randomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    
    
function drawParticles(canvas, massParticles) {    
    for(var i = 0; i< massParticles.length; i++) {
    canvas.beginPath();
    canvas.fillStyle = massParticles[i].color;
    canvas.rect(massParticles[i].Xcoord, massParticles[i].Ycoord, 5, 5);
    canvas.fill();
    canvas.strokeStyle = "#000000";
    canvas.stroke();
    canvas.closePath()
    }
}
    /*
    Diese Funktion stellt zwei Testgegner dar, einer ist anfangs größer, der andere anfangs kleiner als der Spieler.
    */
    function setupEnemies() {
        var lowerEnemy = {
            enemyXcoord: window.innerWidth/2 +200,
            enemyYcoord: window.innerHeight/2 +20,
            enemyRadius: 10,
            enemyColor: randomColor(),
            enemyMass: 10,
        };
        enemies.push(lowerEnemy);
        
        var biggerEnemy = {
            enemyXcoord: window.innerWidth/2 -200,
            enemyYcoord: window.innerHeight/2 -20,
            enemyRadius: 60,
            enemyColor: randomColor(),
            enemyMass: 60
        };
        enemies.push(biggerEnemy);
    };
    
    /*
    Diese Funktion behandelt den Vorgang des "Masse ausstoßen". Minimale Masse beträgt aktuell 30.
    TODO: 30 nicht als Magic Number behandeln!
    */
    function ejectMass() {
        if(player.playerRadius > 30) {
            player.playerRadius = player.playerRadius - 1;
            var massParticle = {Xcoord: player.playerXcoord, Ycoord: player.playerYcoord, color:player.playerColor};
            massEject.push(massParticle);
        }
        /*
        var angle = Math.random()*Math.PI*2;
        var moveToX = Math.cos(angle)*radius;
        var moveToY = Math.sin(angle)*radius; 
        var massSpeed = 4;
        var playerX = player.playerXcoord;
        var playerY = player.playerYcoord;
        var DeltaX = getCoordinateDifference(moveToX, playerX);
        var DeltaY = getCoordinateDifference(moveToY, playerY);
        var Distance = getDistance(DeltaX, DeltaY);
        var speedFactor = getSpeedFactor(Distance, massSpeed);
        var xSpeedMass = getSpeed(DeltaX, speedFactor);
        var ySpeedMass = getSpeed(DeltaY, speedFactor);
        var massEjectParticle = {
            angle: angle,
            playerX: playerX,
            playerY: playerY,
            
        };
        massEject.push(massEjectParticle);
        */
    };
    
    function split() {};
    
    function onKeyDown(event) {
        if(event.which == 87) {
            ejectMass();
        };
        if(event.which == 32) {
            split();
        };
    }


    function init() {
        //create canvas and stuff here
        createMassParticles();
        setupPlayerParameters();
        setupMouseParameters();
        setupEnemies();
    }

    init();
   
    return {
        handleNetwork: handleNetwork,
        handleLogic: handleLogic,
        handleGraphics: handleGraphics,
        handleMouseEvent: handleMouseMovement,
        onKeyDown: onKeyDown
    };
};


