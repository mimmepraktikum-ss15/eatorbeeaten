var express = require('express');
var app     = express();
var config  = require('./config.json');
var http    = require('http').createServer(app).listen(config.port, "0.0.0.0");
var io      = require('socket.io')(http);
var players = {};
var clients = [];
var food    = [];



app.use(express.static(__dirname + '/../client'));

function addFood() {
    //add static particles
};

function removeFood(foodParticleToBeRemoved) {
    //after client checked collision with food send data to server, server removes food  
};

io.on('connection', function (socket) {
    console.log("Somebody connected! ID=" + socket.id + ".");
    clients.push(socket);
    
    socket.on('disconnect', function() {
        var index = clients.indexOf(socket);
        if (index != -1) {
            clients.splice(index, 1);
            console.log("Somebody disconnected! ID=" + socket.id + ".");
        }
    });
    
    socket.on('collision', function (data) {
        //get data from collision
        var newData = 0;
        socket.emit('giveback data', newData);
    });
    
    socket.on('highscore request', function (highscore) {
        //send highscores to clients
        var highscorelist = 0;
        socket.emit('get highscoredata', highscorelist);
    });
    
    
    
});

//var serverPort = process.env.PORT || config.port;
//http.listen(serverPort, function() {
//  console.log("Server is listening on port " + serverPort);
//});
